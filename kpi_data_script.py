#!/usr/bin/python3


"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


# This script captures the capacity and SNR etc. for each UE. \\
# and save file for the data captured. The user needs to provide \\
# number of iterations i.e. number of times in seconds, the data needs to be captured

# To Run the Script
# ./kpi_data_script.py <number of repetitions>

import os
import sys
import time
import json
from datetime import datetime
#from araweather import weather
import subprocess
import getpass
from sklk_sonic.client import JSONZClient

# Port number
PORT = 5555
IP = "10.188.16.1"

# Connecting to Mu2control service on Skylark CU
client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")
try: 
    client.gyro_get_connections()
    pass
except:
    PORT = 5556
    client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")

# Timestamp for file
ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Initialize
data = {}
reps = int(sys.argv[1])

# Directory where the file is stored
PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
if not os.path.exists(PARENT_DIR):
    os.makedirs(PARENT_DIR)
# print(PARENT_DIR)
KPI_DIR = f"{PARENT_DIR}/kpi_data"
# print(KPI_DIR)
# Create direcory if not, to save the csi data
if not os.path.exists(KPI_DIR):
    os.makedirs(KPI_DIR)

# Save connected UEs serials and IPs
serials = []
for ue in client.ue_list():
    if (ue['url'] != None):
        serials.append(ue['serial'])

# Retrieve Frequency and BW in operation
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Collect data and store in a dictionary
for i in range(reps):
    print(f"running iteration: {i+1}")
    # if not (i % 2):
    # client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:" + str(PORT))
    # while True:
    #     try:
    kpis = client.gyro_get_kpis(False)
    time.sleep(0.2)
        #     print("get_kpi_success")
        #     break
        # except:
        #     print("get_kpi_not_success")
        #     time.sleep(1)
    client_stats = client.gyro_get_client_stats()
    ts = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
    data_temp = {}
    data_temp["time_stamp"] = ts

    data_temp["bandwidth"] = bandwidth
    data_temp["frequency"] = frequency

    data_temp["bus_rate_aggregate"] = kpis[0]["aggregate"]["bus"]["avg"]
    data_temp["bus_rate_dl"] = kpis[0]["downlink"]["bus"]["avg"]
    data_temp["bus_rate_ul"] = kpis[0]["uplink"]["bus"]["avg"]

    data_temp["spectral_efficiency"] = kpis[0]["aggregate"]["bus"]["avg"] / bandwidth

    data_temp["siso_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["siso_avg"]
    data_temp["siso_avg_dl"] = kpis[0]["downlink"]["bus"]["siso_avg"]
    data_temp["siso_avg_ul"] = kpis[0]["uplink"]["bus"]["siso_avg"]

    data_temp["mimo_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["mimo_avg"]
    data_temp["mimo_avg_dl"] = kpis[0]["downlink"]["bus"]["mimo_avg"]
    data_temp["mimo_avg_ul"] = kpis[0]["uplink"]["bus"]["mimo_avg"]

    data_temp["per_user_avg_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_avg"]
    data_temp["per_user_avg_dl"] = kpis[0]["downlink"]["bus"]["per_user_avg"]
    data_temp["per_user_avg_ul"] = kpis[0]["uplink"]["bus"]["per_user_avg"]

    data_temp["per_user_max_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_max"]
    data_temp["per_user_max_dl"] = kpis[0]["downlink"]["bus"]["per_user_max"]
    data_temp["per_user_max_ul"] = kpis[0]["uplink"]["bus"]["per_user_max"]

    data_temp["per_user_min_aggregate"] = kpis[0]["aggregate"]["bus"]["per_user_min"]
    data_temp["per_user_min_dl"] = kpis[0]["downlink"]["bus"]["per_user_min"]
    data_temp["per_user_min_ul"] = kpis[0]["uplink"]["bus"]["per_user_min"]

    for j in range(len(serials)):
        data_temp[f"dl_snr_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['snr_0']
        data_temp[f"dl_snr_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['snr_1']
        data_temp[f"ul_snr_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['snr_0']
        data_temp[f"ul_snr_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['snr_1']
        data_temp[f"dl_bus_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['bus']
        data_temp[f"ul_bus_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['bus']
        data_temp[f"dl_decode_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['decode_0']
        data_temp[f"dl_decode_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['decode_1']
        data_temp[f"ul_decode_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['decode_0']
        data_temp[f"ul_decode_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['decode_1']
        data_temp[f"dl_mcs_0_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['mcs_0']
        data_temp[f"dl_mcs_1_{client_stats[j]['serial']}"] = client_stats[j]['downlink']['mcs_1']
        data_temp[f"ul_mcs_0_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['mcs_0']
        data_temp[f"ul_mcs_1_{client_stats[j]['serial']}"] = client_stats[j]['uplink']['mcs_1']

 #   data_temp["weather_data"] = weather_data = weather.get_current_weather(['WilsonHall', 'AgronomyFarm'])
    data[f"sample_{i+1}"] = data_temp
    data1 = json.dumps(data, indent=4)
    with open(f'{KPI_DIR}/kpis_{frequency}_{bandwidth}_{ts_file}.json', 'w') as fp:
        fp.write(data1)
    # Wait for 1 second (Can be increased to collect data at lower intervals)
    time.sleep(1)

# Save to file
print("Saving Data")
#data["connections"] = client.gyro_get_connections()
#data = json.dumps(data, indent=4)
#with open(f'{KPI_DIR}/kpis_{frequency}_{bandwidth}_{ts_file}.json', 'a') as fp:
#    fp.write(data)
print(f"Saved data to {KPI_DIR}")
