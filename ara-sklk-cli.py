#!/usr/bin/python3

"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""


# This is a CLI developed for users to run commands on the AraMIMO BS and monitor the current \\
# state of the system such as the number of users connected, the data rates, the system config \\
# and other stats of the system. Run `help -v` to get help on the available commands.

# runner changes
# more changes
# changes
# changes

import argparse
import csv
import json
import cmd2
from datetime import datetime
from araweather import weather
import requests
import getpass
import os
from cmd2 import Cmd
from sklk_sonic.client import JSONZClient
from tabulate import tabulate

# Port number
PORT = 5555
IP = "10.188.16.1"

# Connecting to Mu2control service on Skylark CU
client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")
try: 
    client.gyro_get_connections()
    pass
except:
    PORT = 5556
    client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")

def format_csv(args, output):
    # Open a CSV file for writing
    csv_file = open(args.csv[0], 'w')

    # Create a CSV writer object
    csv_writer = csv.writer(csv_file)

    # Write the row header
    header = output[0].keys()
    csv_writer.writerow(header)

    # Write each row of data
    for row in output:
        csv_writer.writerow(row.values())

    # Close the csv file
    csv_file.close()


def to_file(args):
    output_file = open(args.file[0], 'w')
    output_file.write(tabulate(table, headers='firstrow'))
    output_file.close()


# class HiddenCommands(cmd2.Cmd):
#     """An app which demonstrates how to hide a command"""
#     def __init__(self):
#         super().__init__()
#         self.hidden_commands.append('shortcuts')


class Prompt(Cmd):
    def __init__(self):
        super().__init__()

        self.hidden_commands.append('shortcuts')
        self.hidden_commands.append('set')
        self.hidden_commands.append('macro')
        self.hidden_commands.append('run_script')
        self.hidden_commands.append('edit')
        self.hidden_commands.append('get_bus_rates')
        self.hidden_commands.append('get_data_rates')

    parser = argparse.ArgumentParser()
    ue_stats_parser = argparse.ArgumentParser()
    snr_parser = argparse.ArgumentParser()
    get_mcs_parser = argparse.ArgumentParser()
    ue_data_rates_parser = argparse.ArgumentParser()
    ue_bus_rates_parser = argparse.ArgumentParser()
    get_data_rates_parser = argparse.ArgumentParser()
    get_bus_rates_parser = argparse.ArgumentParser()
    get_config_parser = argparse.ArgumentParser()
    get_connected_ues_parser = argparse.ArgumentParser()
    get_center_freq_parser = argparse.ArgumentParser()
    get_phy_config_parser = argparse.ArgumentParser()
    get_main_stats_parser = argparse.ArgumentParser()
    csi_parser = argparse.ArgumentParser()
    get_ue_locations_parser = argparse.ArgumentParser()
    get_phy_enabled_parser = argparse.ArgumentParser()
    get_grouping_info_parser = argparse.ArgumentParser()
    get_weather_data_parser = argparse.ArgumentParser()


    ue_stats_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    ue_stats_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    snr_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    snr_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    get_mcs_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    get_mcs_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    ue_bus_rates_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    ue_bus_rates_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    ue_data_rates_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    ue_data_rates_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    get_bus_rates_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    get_bus_rates_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    get_data_rates_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    get_data_rates_parser.add_argument(
        "-c",
        "--csv",
        action="append",
        help="Format file output as a csv file"
    )

    get_config_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_connected_ues_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_ue_locations_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_center_freq_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_phy_config_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_phy_enabled_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    ) 

    get_main_stats_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )

    get_grouping_info_parser.add_argument(
        '-b',
        '--bandno',
        type=int,
        help="band no to get the grouping info for. Enter -1 to get the info for all bands [Between 0 and 41 for 24 MHz BW]"
    )

    get_grouping_info_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
        
    csi_parser.add_argument(
        '-t',
        '--time',
        type=int,
        help="Time in seconds [CAN NOT BE GREATER THAN 100]"
    )
    
    get_weather_data_parser.add_argument(
        "-f",
        "--file",
        action="append",
        help="Send output to the specified file"
    )
    
    get_weather_data_parser.add_argument(
        '-s',
        '--site',
        type=str,
        help="Specify Site to collect weather data for. hint: WilsonHall or AgronomyFarm or all"
    )

    @cmd2.with_argparser(ue_stats_parser)
    def do_get_ue_stats(self, args):
        """Print Connected UE's Statistics"""
        output = client.get_client_stats()
        table = []
        table.append(list(output[0].keys())[-1::-1])
        for item in output:
            if 'bus' in item and 'data' in item:
                item['bus'] = f"{round(item['bus'] / 10 ** 6, 2)}"
                item['data'] = f"{round(item['data'] / 10 ** 6, 2)}"
            table.append(list(item.values())[-1::-1])

        if args.csv:
            format_csv(args, output)
            return

        if args.file:
            to_file(args)
            return

        self.poutput(tabulate(table, headers='firstrow'))

    @cmd2.with_argparser(snr_parser)
    def do_get_snr(self, args):
        """Print Connected UE's SNR"""
        output = client.get_client_stats()
        keys_to_extract = ["serial", "xfer", "snr_0", "snr_1"]

        # dictionary comprehension to create a new list of dictionaries
        new_list = [{k: d[k] for k in keys_to_extract
                     if (k in d) and not (len(d['serial']) == 0) and 'snr_0' in d and 'snr_1' in d}
                    for d in output]

        if args.csv:
            csv_file = open(args.csv[0], 'w')

            csv_writer = csv.writer(csv_file)

            header = new_list[0].keys()
            csv_writer.writerow(header)

            for row in new_list:
                csv_writer.writerow(row.values())

            csv_file.close()

            return

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(tabulate(new_list, headers='firstrow'))
            output_file.close()
            return

        self.poutput(tabulate(new_list, headers="keys"))

    @cmd2.with_argparser(get_mcs_parser)
    def do_get_mcs(self, args):
        """Print average MCS Index for UE's"""
        output = client.get_client_stats()
        keys_to_extract = ["serial", "xfer", "mcs_0", "mcs_1"]

        new_list = [{k: d[k] for k in keys_to_extract
                     if (k in d) and not (len(d['serial']) == 0) and 'mcs_0' in d and 'mcs_1' in d}
                    for d in output]

        # round mcs values
        for d in new_list:
            if 'mcs_0' in d and 'mcs_1' in d:
                d['mcs_0'] = f"{round(d['mcs_0'], 1)}"
                d['mcs_1'] = f"{round(d['mcs_1'], 1)}"

        if args.csv:
            csv_file = open(args.csv[0], 'w')

            csv_writer = csv.writer(csv_file)

            header = new_list[0].keys()
            csv_writer.writerow(header)

            for row in new_list:
                csv_writer.writerow(row.values())

            csv_file.close()

            return

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(tabulate(new_list, headers='firstrow'))
            output_file.close()
            return

        self.poutput(tabulate(new_list, headers="keys"))

    @cmd2.with_argparser(ue_data_rates_parser)
    def do_get_ue_data_rates(self, args):
        """Print Data Rates in Mbps"""
        output = client.get_client_stats()
        keys_to_extract = ["serial", "xfer", "data"]

        new_list = [{k: d[k] for k in keys_to_extract
                     if (k in d) and not (len(d['serial']) == 0) and 'data' in d}
                    for d in output]

        # express data in Mbps
        for d in new_list:
            if 'data' in d:
                d['data'] = f"{round(d['data'] / 10 ** 6, 1)}"

        if args.csv:
            csv_file = open(args.csv[0], 'w')

            csv_writer = csv.writer(csv_file)

            header = new_list[0].keys()
            csv_writer.writerow(header)

            for row in new_list:
                csv_writer.writerow(row.values())

            csv_file.close()

            return

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(tabulate(new_list, headers='firstrow'))
            output_file.close()
            return

        self.poutput(tabulate(new_list, headers="keys"))

    @cmd2.with_argparser(ue_bus_rates_parser)
    def do_get_ue_bus_rates(self, args):
        """Print Bus Rates in Mbps"""
        output = client.get_client_stats()
        keys_to_extract = ["serial", "xfer", "bus"]

        new_list = [{k: d[k] for k in keys_to_extract
                     if (k in d) and not (len(d['serial']) == 0) and 'bus' in d}
                    for d in output]

        # express bus rate in Mbps
        for d in new_list:
            if 'bus' in d:
                d['bus'] = f"{round(d['bus'] / 10 ** 6, 1)}"

        if args.csv:
            csv_file = open(args.csv[0], 'w')

            csv_writer = csv.writer(csv_file)

            header = new_list[0].keys()
            csv_writer.writerow(header)

            for row in new_list:
                csv_writer.writerow(row.values())

            csv_file.close()

            return

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(tabulate(new_list, headers='firstrow'))
            output_file.close()
            return

        self.poutput(tabulate(new_list, headers="keys"))

    @cmd2.with_argparser(get_config_parser)
    def do_get_config(self, args):
        """Return Current BS Configuration"""
        output = client.get_config()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(get_connected_ues_parser)
    def do_get_connected_ues(self, args):
        """Returns all the connected ues"""
        output = client.gyro_get_connections()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(get_ue_locations_parser)
    def do_get_ue_locations(self, args):
        """Returns the locations of ues"""
        #output = client.gyro_get_connections()

        ue_locations = {
            "FL1B000005":  "AgronomyFarm-UE-010",
            "FL1B000006":  "CurtissFarm-UE-000",
            "FL1B000011":  "Ames-UE-000",
            "FL1B000013":  "KitchenFarm-UE-000",
            "FL1B000016":  "SheepFarm-UE-000",
            "FL1B000017":  "DairyFarm-UE-000",
            "FL1B000025":  "CurtissFarm-UE-001",
            "FL1B000030":  "CurtissFarm-UE-002",
            "FL1B000019":  "Ames-UE-140",
            "FL1B000032":  "FeedMill-UE-000",
            "FL1B000031":  "BeefFarm-UE-000"
        }

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(ue_locations, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(ue_locations, indent=4))

    @cmd2.with_argparser(get_center_freq_parser)
    def do_get_center_freq(self, args):
        """Return Center Frequency on all Radios"""
        output = client.get_center_freq()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(get_phy_config_parser)
    def do_get_phy_config(self, args):
        """Print Phy Configuration Parameters"""
        output = client.get_phy_config()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(get_phy_enabled_parser)
    def do_get_phy_enabled(self, args):
        """Check if Phy is Enabled and BS Transmitting"""
        output = client.gyro_get_phy_enabled()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(output)

    @cmd2.with_argparser(get_main_stats_parser)
    def do_get_main_stats(self, args):
        """Returns detailed statistics of the system"""
        output = client.gyro_get_main_stats()

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(get_grouping_info_parser)
    def do_get_grouping_info(self, args):
        """Returns detailed grouping info at each band"""
        band = args.bandno
        output = client.gyro_get_group_summary(band)

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(output, indent=4))

    @cmd2.with_argparser(csi_parser)
    def do_get_csi_data(self, args):
        """Returns csi data"""
        # Generate timestamp
        ts = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
        PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
        if not os.path.exists(PARENT_DIR):
            os.makedirs(PARENT_DIR)
        # Send API request for csi data
        url = f'http://{IP}:8000/csi_dump'
        csi_time = args.time
        data = {
            'csi_time': csi_time
        }
        response = requests.post(url, json=data)
        if response.headers.get('content-type') == 'application/zip':
            # Response is a ZIP archive, save it locally
            with open(f'{PARENT_DIR}/csi_{ts}.zip', 'wb') as file:
                file.write(response.content)
        print(f"Data Saved in {PARENT_DIR}")

    @cmd2.with_argparser(get_weather_data_parser)
    def do_get_weather_data(self, args):
        """Return Weather Data for Wilson Hall and Agronomy Farm"""
        sitename = args.site
        if sitename == 'all':
            weather_data = weather.get_current_weather(['WilsonHall', 'AgronomyFarm'])
        else:
            weather_data = weather.get_current_weather([sitename])

        if args.file:
            output_file = open(args.file[0], 'w')
            output_file.write(json.dumps(output, indent=4))
            output_file.close()
            return

        self.poutput(json.dumps(weather_data, indent=4))


    def emptyline(self):
        self.poutput(' ')


if __name__ == '__main__':
    prompt = Prompt()
    prompt.prompt = "ara-sklk-sh> "
    prompt.cmdloop()
