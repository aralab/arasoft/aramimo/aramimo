#!/usr/bin/python3


"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

# This script records the received signal strength for all UEs. The user needs to provide \\
# number of reps for which the signal strength has be recorded.

# To Run the Script
# ./ue_stats_script.py <number of repetitions>

import os
import sys
import time
import json
from datetime import datetime
import threading
import requests
import getpass
import ast
from sklk_sonic.client import JSONZClient

# Port number
PORT = 5555
IP = "10.188.16.1"

# Connecting to Mu2control service on Skylark CU
client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")
try: 
    client.gyro_get_connections()
    pass
except:
    PORT = 5556
    client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")

# Timestamp for file
ts_dir = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")

# Initialize
reps = int(sys.argv[1])

# Retrieve Frequency and BW in operation
connections = client.gyro_get_connections()
time.sleep(0.5)
config = client.get_config()
bandwidth = config[1]["radio_config"]["filter_bw"]
frequency = config[1]["radio_config"]["frequency"]

# Directory where the files are stored
PARENT_DIR = f"/home/{getpass.getuser()}/sklk_data"
if not os.path.exists(PARENT_DIR):
    os.makedirs(PARENT_DIR)
DIR = f"ue_stats_{frequency}_{bandwidth}_{ts_dir}"
UE_STATS_DIR = f"{PARENT_DIR}/ue_stats_data"
# Create direcory if not, to save the csi data
if not os.path.exists(UE_STATS_DIR):
    os.makedirs(UE_STATS_DIR)
PATH = os.path.join(UE_STATS_DIR, DIR)
os.mkdir(PATH)

time.sleep(0.5)
# Save connected UEs serials and IPs
serials = []
for ue in client.ue_list():
    if (ue['url'] != None):
        serials.append(ue['serial'])
# Collect data for UE


def run_command(serial, reps):
    ts_file = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
    url = f'http://{IP}:8000/ue_stats'
    arg_send = {
        'serial':  serial,
        'reps':  reps
    }
    arg_send = json.dumps(arg_send)
    output = requests.post(url, data=arg_send, headers={
                           'Content-Type': 'application/json'})
    # print(output.json())
    # Save to file
    with open(f'{UE_STATS_DIR}/{DIR}/ue_stats_{serial}_{frequency}_{bandwidth}_{ts_file}.json', 'w') as fp:
        json.dump(output.json(), fp, indent=4)

# Start thread for each UE
threads = []
for j in range(len(serials)):
    thread = threading.Thread(
        target=run_command,
        args=(serials[j], reps)
    )
    thread.start()
    threads.append(thread)
    time.sleep(1)

# Wait for all threads to finish
for thread in threads:
    thread.join()

print("Trace finished")
print(f"Data Saved to {UE_STATS_DIR}")
