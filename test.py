#!/usr/bin/python3

"""
   Copyright [2023] [Center for Wireless Communities and Innovation (WiCI), Iowa State University]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
"""

from sklk_sonic.client import JSONZClient

# Port number
PORT = 5555
#IP = "10.188.16.1"
IP = "10.189.0.134"

# Connecting to Mu2control service on Skylark CU
client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")
try: 
    client.gyro_get_connections()
    pass
except:
    PORT = 5556
    client = JSONZClient("Mu2 Control", url=f"tcp://{IP}:{PORT}")

output = client.gyro_get_connections()
print(output)
